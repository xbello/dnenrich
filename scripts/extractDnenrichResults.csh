#!/bin/csh -f

#set P_VAL_THRESH = 0.05
set P_VAL_THRESH = 1

#set RECURRENCE_LEVEL = "2"
set RECURRENCE_LEVEL = "*"

set SORT_OUTPUT = 1

################################################################################
# Following:
# http://software.frodo.looijaard.name/getopt/docs/getopt-parse.tcsh
################################################################################

# This is a bit tricky. We use a temp variable, to be able to check the
# return value of getopt (eval nukes it). argv contains the command arguments
# as a list. The ':q`  copies that list without doing any substitutions:
# each element of argv becomes a separate argument for getopt. The braces
# are needed because the result is also a list.
set temp=(`getopt -s tcsh -o anb:p:r:c:: --long a-long,noSortOutput,b-long:,p-val:,recurrence:,c-long:: -- $argv:q`)
if ($? != 0) then 
  echo "Terminating..." >/dev/stderr
  exit 1
endif

# Now we do the eval part. As the result is a list, we need braces. But they
# must be quoted, because they must be evaluated when the eval is called.
# The 'q` stops doing any silly substitutions.
eval set argv=\($temp:q\)

#echo "B/4: $*"

while (1)
	switch($1:q)
	case -a:
	case --a-long:
		echo "Option a" ; shift 
		breaksw;
	case -n:
	case --noSortOutput:
		set SORT_OUTPUT = 0; shift 
		breaksw;
	case -b:
	case --b-long:
		echo "Option b, argument "\`$2:q\' ; shift ; shift
		breaksw
	case -c:
	case --c-long:
		# c has an optional argument. As we are in quoted mode,
		# an empty parameter will be generated if its optional
		# argument is not found.

		if ($2:q == "") then
			echo "Option c, no argument"
		else
			echo "Option c, argument "\`$2:q\'
		endif
		shift; shift
		breaksw
	case --:
		shift
		break
	case -p:
	case --p-val:
		set P_VAL_THRESH = $2:q ; shift ; shift
		breaksw
	case -r:
	case --recurrence:
		set RECURRENCE_LEVEL = $2:q ; shift ; shift
		breaksw
	default:
		echo "Internal error!" ; exit 1
	endsw
end


#echo "AFTER: $*"


if ( $#argv == 0 ) then
	echo "ERROR: Usage: extractDnenrichResults.csh dnenrich_results_files" > /dev/stderr
	exit
else if ( $#argv >= 1 ) then
    set RESULTS_FILES = "$*"
endif
################################################################################


#echo $RESULTS_FILES


set outPipe = ./DN_ENRICH.$$__$$


set START_RECURRENT_GENES_FIELD = 8

#
# split(NUM_MUTS,a,"/"); NUM_MUTS=a[1];
#

awk '{split(FILENAME, a, "/"); baseName=a[length(a)]; split(baseName, a, "-"); setContextName="."; if (length(a) >= 2) {setContextName=a[2]; for (i=3; i<=length(a)-2; i++) {setContextName=setContextName"-"a[i]}} group="."; if (length(a) > 1) {group=a[length(a)-1]} mutType=a[length(a)]; print $_, group, mutType, setContextName}' $RESULTS_FILES | \
\
awk 'BEGIN{OFS="\t"; \
CUR_SET=-1; prevGene=""; genes[1]=1; \
} \
\
function addPrevGene() { \
if (prevGene != "") {genes[prevGene]++; prevGene=""} \
} \
\
function printGenes() { \
addPrevGene(); \
if (CUR_SET != -1) { \
s = ""; n=asorti(genes, indices); numMutsHit=0; for (i=1; i<=n; i++) {gene=indices[i]; numMutsHit+=genes[gene]; if (i > 1) {s=s" "} s = s""gene; if (genes[gene] > 1) {s=s"(x"genes[gene]")"}} \
print CUR_SET,numMutsHit,s; \
} \
CUR_SET=-1; \
delete genes; \
} \
\
{ \
if ($1 == "__GRP1") {if (CUR_SET != -1) {geneOverlapIndex=$6; gene=$8; if (geneOverlapIndex == 1) {addPrevGene()} if (prevGene != "") {prevGene=prevGene"+"} prevGene=prevGene""gene}} \
else if ($1 == "__GRP0") { } \
else { \
pVal=$3; testRecurrence=$2; \
if ((("'"$RECURRENCE_LEVEL"'" == "*" && testRecurrence == "'"$RECURRENCE_LEVEL"'") || ("'"$RECURRENCE_LEVEL"'" != "*" && testRecurrence >= "'"$RECURRENCE_LEVEL"'")) && pVal <= '$P_VAL_THRESH') { \
printGenes(); \
numGenesSet=$6; numHitsSet=$4; expectedNumHitsSet=$5; set=$1; NUM_MUTS=$7; NUM_GENES_IN_MUTS=$8; \
if (testRecurrence != "*") { set=set">="testRecurrence } \
group=$(NF-2); mutType=$(NF-1); setContext=$NF; \
CUR_SET=group""OFS""mutType""OFS""NUM_MUTS""OFS""NUM_GENES_IN_MUTS""OFS""setContext""OFS""set""OFS""pVal""OFS""numGenesSet""OFS""numHitsSet""OFS""expectedNumHitsSet; \
delete genes; \
if (testRecurrence != "*") {for (i = '$START_RECURRENT_GENES_FIELD'; i <= NF - 3; i++) {split($i,a,"("); genes[a[1]]++}; printGenes(); } \
} \
} \
} \
END{printGenes(); }' \
>! $outPipe



echo "" | awk 'BEGIN{OFS="\t"; print "GROUP","MUT_TYPE","NUM_MUTS","NUM_GENES_IN_MUTS","SET_CONTEXT","SET","P_VAL","NGENES_SET","OBS_HITS_STAT","EXP_HITS_STAT","NUM_MUTS_HIT","GENES"}'

if ($SORT_OUTPUT) then
	cat $outPipe | \
	sort -k6 -g
else
	cat $outPipe
endif

rm -f $outPipe
