Software for assessment of statistical enrichment of de novo mutations in gene sets.

## Web site: ##
# **https://psychgen.u.hpc.mssm.edu/dnenrich** #



![dnenrich_dartboard.png](https://bitbucket.org/repo/LrLooM/images/3670278451-dnenrich_dartboard.png)